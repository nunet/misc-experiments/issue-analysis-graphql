import requests
from slugify import slugify 

def run_query(uri: str, query, statusCode, headers, method="POST"):
    request = requests.request(method, uri, json={'query': query}, headers=headers)
    # print(f"Epic name at this point is {epic}")
    if request.status_code == statusCode:
        return request.json()
    else:
        raise Exception(f"Unexpected status code returned: {request.status_code}")

gitlabURI = 'https://gitlab.com/api/graphql'
gitlabToken = 'glpat-1iSZ6gRkyzL6vzzgiZox'
gitlabHeaders = {"Authorization": "Bearer " + gitlabToken}
gitlabStatusCode = 200

def processTitle(title):
    title = title.replace('[', '/')
    title = title.replace(']', '/')
    title = title.replace('(', '/')
    title = title.replace(')', '/')
    words = title.split()
    processedTitle = []
    x = 0
    max = 6
    for word in words:
        processedTitle.append(word)
        if x == max:
            processedTitle.append('<br/>')
            x = 0
        x = x + 1
    return ' '.join(processedTitle)

def processLabels(labels):
    if len(labels) > 0:
        for label in labels:
            if 'ccpm::task' in label['title']:
                return 'ccpm'
            if 'ccpm::project_buffer' in label['title']:
                return 'ccpm'
            if 'Priority::High' in label['title']:
                return 'High'
    return ''

def writeMermaidIssue(issue, f):
    id = issue['id'].split('/')[4]
    title = issue['title']
    title = processTitle(title)
    webUrl = issue['webUrl']
    state = issue['state']
    ttype = issue['type']
    # getting labels
    labels = ''
    if 'labels' in issue:
        labels = processLabels(issue['labels']['nodes'])
    # gitlab graphql does not return the closed issues in the blockedByIssues collection
    # ignoring closed issues since they appear disconnected in the mermaid graph
    # ignoring types that are not ISSUE
    if 'closed' not in state and 'ISSUE' in ttype:
        mermaidIssue = (id+'([<a href=\''+webUrl+'\'>'+ title +'</a>])')
        if 'Milestone goal' in title:
            mermaidIssue += ':::goal'
        else:
            if 'Milestone' in title:
                mermaidIssue += ':::milestone'
            else:
                if 'High' in labels:
                    mermaidIssue += ':::high'
                else:
                    if 'ccpm' in labels:
                        mermaidIssue += ':::goal'
        f.write(mermaidIssue)
        f.write('\n')

def writeMermaidRelation(id1,id2,f):
    relation = (id1+'-->'+id2)
    f.write(relation)
    f.write('\n')

def processIssues(issues):
    for issue in issues:
        writeMermaidIssue(issue, f)
        blockedByIssues = []
        if 'blockedByIssues' in issue:
            blockedByIssues = issue['blockedByIssues']['nodes']
        if len(blockedByIssues) > 0:
            processIssues(blockedByIssues)

def processRelations(issues):
    for issue in issues:
        id1 = issue['id'].split('/')[4]
        if 'blockedByIssues' in issue:
            blockedByIssues = issue['blockedByIssues']['nodes']
            for blockedBy in blockedByIssues:
                id2 = blockedBy['id'].split('/')[4]
                writeMermaidRelation(id2,id1,f)

milestones = run_query("https://gitlab.com/api/v4/groups/6160918/milestones?state=active", "", gitlabStatusCode, {"PRIVATE-TOKEN": gitlabToken}, "GET")

for milestone in milestones:
    epic = milestone["title"]
    gitlabQuery = """\
      query {
        group(fullPath: "nunet") {
            issues(milestoneTitle: """ + f'"{epic}"' + """) {
                nodes {
                    ...nodeProperties
                    blockedByIssues {
                        nodes {
                            ...nodeProperties
                            blockedByIssues {
                                nodes {
                                    ...nodeProperties
                                }
                            }
                        }
                    }
                }
            }
        }
    }

fragment nodeProperties on Issue {
    id
    webUrl
    title
    iteration {title}
    epic {id title}
    labels {nodes {title}}
    assignees {nodes {name}}
    updatedBy {name}
    updatedAt
    weight
    state
    type
}
"""
    result = run_query(gitlabURI, gitlabQuery, gitlabStatusCode, gitlabHeaders)
    #print(result)
    issues = result['data']['group']['issues']['nodes']
    #print(issues)
    f = open(f"{slugify(milestone['title'])}.mermaid", "w")
    f.write('graph LR;\n')
    f.write('\n')
    processIssues(issues)
    processRelations(issues)
    f.write('classDef high stroke:red,stroke-width:2px;\n')
    f.write('classDef goal fill:white,color:black,stroke:green,stroke-width:4px;\n')
    f.write('classDef milestone fill:white,color:blue,stroke:blue,stroke-width:4px;\n')
    f.close()
