import requests
from slugify import slugify
from datetime import timedelta, date, datetime
import math

def run_query(uri: str, query, statusCode, headers, method="POST"):
    request = requests.request(method, uri, json={'query': query}, headers=headers)
    # print(f"Epic name at this point is {epic}")
    if request.status_code == statusCode:
        return request.json()
    else:
        raise Exception(f"Unexpected status code returned: {request.status_code}")

gitlabURI = 'https://gitlab.com/api/graphql'
gitlabToken = 'glpat-1iSZ6gRkyzL6vzzgiZox'
gitlabHeaders = {"Authorization": "Bearer " + gitlabToken}
gitlabStatusCode = 200

def excludeWeekends(start_dt, end_dt):

    weekends = []
    def daterange(date1, date2):
        for n in range(int ((date2 - date1).days)+1):
            yield date1 + timedelta(n)

    weekdays = [5,6]
    for dt in daterange(start_dt, end_dt):
        if dt.weekday() in weekdays:                    # to print only the weekdates
            weekends.append(dt.strftime("%Y-%m-%d"))

    return weekends

# issue ids sorted by their start date (for formatting the ganttt chart);
ccpm_issues_sorted = []

# map of all issues; key- issue id; value -- all metadata;
ccpm_issues = {}

# project buffer;
ccpm_buffer = []

# first issue that starts the project;
ccpm_start = []


# sorts the list of the issues by dependencies;
def sortIssues(lastIssue, issuesList):
    blockedByIssuesList = []
    if 'blockedByIssues' in lastIssue:
        blockedByIssuesIdList = lastIssue['blockedByIssues']['nodes']
        for blockedByIssueId in blockedByIssuesIdList:
            blockedBy=issuesList[blockedByIssueId["id"]]
            labels = blockedBy['labels']['nodes']
            for label in labels:
                if 'ccpm::task' in label['title'] or 'ccpm::start' in label['title']:
                    ccpm_issues[blockedByIssueId['id']] = blockedBy
                    ccpm_issues_sorted.insert(0,blockedByIssueId['id'])
            sortIssues(blockedBy,issuesList)

def getIssue(id):
    gitLabQuery = """\
      query {
            issue(id: """ + f'"{id}"' + """) {
            ...nodeProperties
            }
      }

    fragment nodeProperties on Issue {
        id
        webUrl
        title
        iteration {title}
        epic {id title}
        labels {nodes {title}}
        assignees {nodes {name}}
        updatedBy {name}
        updatedAt
        weight
        state
        dueDate
        blockedByIssues {
          nodes {
            id
          }
        }
    }
    """
    result = run_query(gitlabURI, gitLabQuery, gitlabStatusCode, gitlabHeaders)
    issue = result['data']['issue']
    return issue

# gets tasks from the milestone based on the milestone id
# and on the fact that all tasks are in the architecture repo
def getTasks(milestone):
    state = "opened"
    gitlabQuery = """\
        query {
                project(fullPath: "nunet/architecture") {
                   issues(milestoneTitle: """ + f'"{milestone}"' + """, state: """ + f'{state}' + """) {
                        nodes {
                            ...nodeProperties
                            }
                        }
                    }
                }

        fragment nodeProperties on Issue {
            id
            webUrl
            title
            iteration {title}
            epic {id title}
            labels {nodes {title}}
            assignees {nodes {name}}
            updatedBy {name}
            updatedAt
            weight
            state
            dueDate
            blockedByIssues {
            nodes {
                id
            }
            }
        }
        """
    
    result = run_query(gitlabURI, gitlabQuery, gitlabStatusCode, gitlabHeaders)
    tasks = result['data']['project']['issues']['nodes']

    return(tasks)

def getMilestoneIssues(milestone):
    gitlabQuery = """\
      query {
        group(fullPath: "nunet") {
            issues(milestoneTitle: """ + f'"{milestone}"' + """) {
                nodes {
                    ...nodeProperties
                    }
                }
            }
    }

    fragment nodeProperties on Issue {
        id
        webUrl
        title
        iteration {title}
        epic {id title}
        labels {nodes {title}}
        assignees {nodes {name}}
        updatedBy {name}
        updatedAt
        weight
        state
        dueDate
        blockedByIssues {
          nodes {
            id
          }
        }
    }
    """
    result = run_query(gitlabURI, gitlabQuery, gitlabStatusCode, gitlabHeaders)
    #print(result)
    issues = result['data']['group']['issues']['nodes']

    return(issues)


# get all active milestones
milestones = run_query("https://gitlab.com/api/v4/groups/6160918/milestones?state=active", "", gitlabStatusCode, {"PRIVATE-TOKEN": gitlabToken}, "GET")

# iterate over all milestones and update their gannt chart
for milestone in milestones:

    # reset global variables
    # (this is not done well these variables need to be properly passed to functions)
    ccpm_issues_sorted = []
    ccpm_issues = {}
    ccpm_buffer = []
    ccpm_start = []

    n = open(f"{slugify(milestone['title'])}-notes.mermaid", "w")
    n.write("Notes to attend for project planning:\n")

    if milestone['start_date'] is None:
        title = milestone['title']
        n.write("Milestone "+ title + " does not have start date -- assuming that it is not started/planned, therefore skipping\n")
    else:
        tasks = getMilestoneIssues(milestone['title'])
        for task in tasks:
            ccpm_issues[task['id']] = task
            for label in task['labels']['nodes']:
                if 'ccpm::project_buffer' in label['title']:
                    ccpm_buffer.append(task)
                if 'ccpm::start' in label['title']:
                    ccpm_start.append(task)

        if len(ccpm_buffer) is 0:
            n.write("Milestone "+ title + " does not contain buffer task; assuming it is not planned yet and aborting\n")
            
        else:
            sortIssues(ccpm_buffer[0],ccpm_issues)
        
            f = open(f"{slugify(milestone['title'])}-gantt.mermaid", "w")
            f.write('gantt\n')
            f.write('\n')
            f.write('title '+ milestone['title']+'\n')
            f.write('dateFormat YYYY-MM-DD' + '\n')
            f.write('excludes weekends\n')
            f.write('section ' + milestone['title']+'\n')
            project_duration = 0
            for id in range(0,len(ccpm_issues_sorted)):
                issueId = ccpm_issues_sorted[id]
                task = ccpm_issues[issueId]
                name = task['title']
                duration = task['weight']
                if duration is None:
                    duration = 30
                    n.write("Task " + name + " does not have duration / weight assigned;\nPlease asign correct duration to the task in order for for the project buffer and gantt chart to be calculated correctly and then rerun;\n30 days default duration is now assigned to pass the script;\n")
                project_duration = project_duration + duration
                milestone_start_date = datetime.strptime(milestone['start_date'], '%Y-%m-%d')
                task_start_date = milestone_start_date
                if id == 0:
                    f.write(name + '\t:' + 'a' + str(id) + ', ' + task_start_date.strftime('%Y-%m-%d') + ', ' + str(duration) + 'd' + '\n')
                else: 
                    f.write(name + '\t:' + 'a' + str(id) + ', after a' + str(id-1) + ' ' + ', ' + str(duration) + 'd' + '\n')

            id = len(ccpm_issues_sorted)
            task = ccpm_buffer[0]
            name = task['title']
            if task['weight'] is None:
                duration = math.ceil(project_duration / 3)
                n.write("Calculated project buffer duration is "+ str(duration)+ " days; Please put set this duration is the task "+name+" and set the start date to the one indicated in the generalted gantt chart;\nthen please rerun the ci to update the gantt chart.\n")
                f.write(name + '\t:' + 'a' + str(id) + ', after a' + str(id-1) + ' ' + ', ' + str(duration) + 'd' + '\n')        
            else:
                duration = task['weight']
                start_date = task['start_date']
                f.write(name + '\t:' + 'a' + str(id) + ', ' + task_start_date.strftime('%Y-%m-%d') + ', ' + str(duration) + 'd' + '\n')
                

            # TODO: make a gantt chart from issues and then add buffer at the end;
            # TODO: get the other issues and get their dependencies;

            f.close()
            n.close()

for milestone in milestones:
    # TODO: make a file for publishing to the repo so that gitbook could pick it up;
    # TODO: make a small csv file for each milestone for containing 
    print(NotImplemented)

